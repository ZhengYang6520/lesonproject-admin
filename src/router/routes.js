import layoutHeaderAside from '@/layout/header-aside'

// 由于懒加载页面太多的话会造成webpack热更新太慢，所以开发环境不使用懒加载，只有生产环境使用懒加载
const _import = require('@/libs/util.import.' + process.env.NODE_ENV)

/**
 * 在主框架内显示
 */
const frameIn = [{
  path: '/',
  redirect: {
    name: 'index'
  },
  component: layoutHeaderAside,
  children: [
    // 首页
    {
      path: 'index',
      name: 'index',
      meta: {
        auth: true
      },
      component: _import('system/index')
    },
    //商品界面
    {
      path: 'product',
      name: 'product',
      meta: {
        title: '产品类表',
        auth: true
      },
      component: _import('product/product')
    },
    {
      path: 'addProduct',
      name: 'addProduct',
      meta: {
        title: '添加商品',
        auth: true
      },
      component: _import('product/product/addProduct')
    },
    {
      path: 'productCategory',
      name: 'productCategory',
      meta: {
        title: '分类管理',
        auth: true
      },
      component: _import('product/productCategory')
    },
    {
      path: 'productBrand',
      name: 'productBrand',
      meta: {
        title: '品牌管理',
        auth: true
      },
      component: _import('product/productBrand')
    },
    {
      path: 'addBrand',
      name: 'addBrand',
      meta: {
        title: '品牌管理',
        auth: true
      },
      component: _import('product/productBrand/addBrand')
    },
    {
      path: 'product/mallSet',
      name: 'mallSet',
      meta: {
        title: '品牌管理',
        auth: true
      },
      component: _import('product/mallSet')
    },
    //摇奖
    {
      path: 'promotion/lottery',
      name: 'lottery',
      meta: {
        title: '摇奖',
        auth: true
      },
      component: _import('promotion/lottery')
    },
    {
      path: '/lottery/prizeinfo',
      name: 'prizeinfo',
      meta: {
        title: '奖品列表',
        auth: true
      },
      component: _import('promotion/lottery/prizeinfo')
    },
    {
      path: 'promotion/coupon',
      name: 'coupon',
      meta: {
        title: '优惠券',
        auth: true
      },
      component: _import('promotion/coupon')
    },
    {
      path: 'coupon/addCoupon',
      name: 'addCoupon',
      meta: {
        title: '添加优惠券',
        auth: true
      },
      component: _import('promotion/coupon/addCoupon')
    },
    {
      path: 'promotion/pointsmall',
      name: 'pointsmall',
      meta: {
        title: '积分兑换',
        auth: true
      },
      component: _import('promotion/pointsmall')
    },
    {
      path: 'promotion/gift',
      name: 'gift',
      meta: {
        title: '礼品',
        auth: true
      },
      component: _import('promotion/gift')
    },
    //分销管理
    {
      path: 'distribution/distributors',
      name: 'distributors',
      meta: {
        title: '分销员',
        auth: true
      },
      component: _import('distribution/distributors')
    },
    {
      path: 'distribution/distributionManage',
      name: 'distributionManage',
      meta: {
        title: '分销设置',
        auth: true
      },
      component: _import('distribution/distributionManage')
    },
    {
      path: 'distribution/subdistributors',
      name: 'subdistributors',
      meta: {
        title: '下级分销商',
        auth: true
      },
      component: _import('distribution/subdistributors')
    },
    {
      path: 'distribution/commissionReport',
      name: 'commissionReport',
      meta: {
        title: '下级分销商',
        auth: true
      },
      component: _import('distribution/commissionReport')
    },
    {
      path: 'distribution/distributionExamine',
      name: 'distributionExamine',
      meta: {
        title: '提现审核',
        auth: true
      },
      component: _import('distribution/distributionExamine')
    },
    //订单管理
    {
      path: 'productOrder',
      name: 'productOrder',
      meta: {
        title: '订单列表',
        auth: true
      },
      component: _import('order/productOrder')
    },
    {
      path: '/productOrder/printOrder',
      name: 'productOrder',
      meta: {
        title: 'printOrder',
        auth: true
      },
      component: _import('order/productOrder/printOrder')
    },
    //抢购
    {
      path: '/takemoney/takemoney',
      name: 'takemoney',
      meta: {
        title: '抢购列表',
        auth: true
      },
      component: _import('takemoney/takemoney')
    },
    {
      path: '/takemoney/addpanicbuy',
      name: 'addpanicbuy',
      meta: {
        title: '添加抢购',
        auth: true
      },
      component: _import('takemoney/addpanicbuy')
    },
    {
      path: '/takemoney/diamond',
      name: 'diamond',
      meta: {
        title: '钻石列表',
        auth: true
      },
      component: _import('takemoney/diamond')
    },
    {
      path: '/takemoney/diamond/diamondprice',
      name: 'diamondprice',
      meta: {
        title: '钻石价格',
        auth: true
      },
      component: _import('takemoney/diamond/diamondprice')
    },
    {
      path: '/takemoney/moneySetting',
      name: 'moneySetting',
      meta: {
        title: '抢购设置',
        auth: true
      },
      component: _import('takemoney/moneySetting')
    },
    //文章界面
    {
      path: 'articleCategory',
      name: 'articleCategory',
      meta: {
        title: '文章分类',
        auth: true
      },
      component: _import('article/articleCategory')
    },
    {
      path: 'articleList',
      name: 'articleList',
      meta: {
        title: '文章列表',
        auth: true
      },
      component: _import('article/articleList')
    },
    {
      path: 'addArticle',
      name: 'addArticle',
      meta: {
        title: '添加文章',
        auth: true
      },
      component: _import('article/addArticle')
    },
    {
      path: 'newsCategory',
      name: 'newsCategory',
      meta: {
        title: '新闻分类',
        auth: true
      },
      component: _import('article/newsCategory')
    },
    {
      path: 'newsList',
      name: 'newsList',
      meta: {
        title: '新闻列表',
        auth: true
      },
      component: _import('article/newsList')
    },
    {
      path: 'addNews',
      name: 'addNews',
      meta: {
        title: '添加新闻',
        auth: true
      },
      component: _import('article/addNews')
    },
    // 会员界面
    {
      path: 'userList',
      name: 'userList',
      meta: {
        title: '会员列表',
        auth: true
      },
      component: _import('users/userList')
    },
    {
      path: 'userLevel',
      name: 'userLevel',
      meta: {
        title: '会员等级',
        auth: true
      },
      component: _import('users/userLevel')
    },
    {
      path: 'users/userSet',
      name: 'userSet',
      meta: {
        title: '会员等级',
        auth: true
      },
      component: _import('users/userSet')
    },
    //图片管理
    {
      path: 'picture/advertising',
      name: 'advertising',
      meta: {
        title: '轮播广告',
        auth: true
      },
      component: _import('picture/advertising')
    },
    {
      path: 'advertising/addAdvertising',
      name: 'addAdvertising',
      meta: {
        title: '广告添加',
        auth: true
      },
      component: _import('picture/advertising/addAdvertising')
    },
    {
      path: 'picture/classification',
      name: 'classification',
      meta: {
        title: '分类管理',
        auth: true
      },
      component: _import('picture/classification')
    },

    //管理员界面
    {
      path: 'adminList',
      name: 'adminList',
      meta: {
        title: '管理员列表',
        auth: true
      },
      component: _import('admin/adminList')
    },
    {
      path: 'manageGroups',
      name: 'manageGroups',
      meta: {
        title: '管理组',
        auth: true
      },
      component: _import('admin/manageGroups')
    },
    {
      path: 'duties',
      name: 'duties',
      meta: {
        title: '职务',
        auth: true
      },
      component: _import('admin/duties')
    },
    //多商户管理
    {
      path: 'merchant',
      name: 'merchant',
      meta: {
        title: '商户列表',
        auth: true
      },
      component: _import('merchant/merchant')
    },
    {
      path: 'merchantLevel',
      name: 'merchantLevel',
      meta: {
        title: '商家等级',
        auth: true
      },
      component: _import('merchant/merchantLevel')
    },
    {
      path: 'merchantNature',
      name: 'merchantNature',
      meta: {
        title: '商户列表',
        auth: true
      },
      component: _import('merchant/merchantNature')
    },
    {
      path: 'merchant/merchantSpace',
      name: 'merchantSpace',
      meta: {
        title: '商户列表',
        auth: true
      },
      component: _import('merchant/merchantSpace')
    },
    {
      path: 'merchant/merchantDetail',
      name: 'merchantDetail',
      meta: {
        title: '商户列表',
        auth: true
      },
      component: _import('merchant/merchantDetail')
    },
    //系统设置
    {
      path: 'settings',
      name: 'settings',
      meta: {
        title: '站点设置',
        auth: true
      },
      component: _import('system/website/settings')
    },
    {
      path: 'website/onlineSetting',
      name: 'onlineSetting',
      meta: {
        title: '网赚设置',
        auth: true
      },
      component: _import('system/website/onlineSetting')
    },
    {
      path: 'website/mobileRouting',
      name: 'mobileRouting',
      meta: {
        title: '移动路由设置',
        auth: true
      },
      component: _import('system/website/mobileRouting')
    },
    {
      path: 'website/siteGuide',
      name: 'siteGuide',
      meta: {
        title: '网务管理',
        auth: true
      },
      component: _import('system/website/siteGuide')
    },
    {
      path: 'payment',
      name: 'payment',
      meta: {
        title: '支付设置',
        auth: true
      },
      component: _import('system/pay/payment')
    },
    {
      path: 'commonarea',
      name: 'commonarea',
      meta: {
        title: '支付设置',
        auth: true
      },
      component: _import('system/commonarea')
    },
    {
      path: 'website/sensitiveWord',
      name: 'sensitiveWord',
      meta: {
        title: '敏感词设置',
        auth: true
      },
      component: _import('system/website/sensitiveWord')
    },
    {
      path: 'system/notice',
      name: 'notice',
      meta: {
        title: '站点公告',
        auth: true
      },
      component: _import('system/notice')
    },
    //页面装修
    {
      path: 'webside',
      name: 'webside',
      meta: {
        title: '网页端首页',
        auth: true
      },
      component: _import('admin/renovation/webside')
    },
    {
      path: '/admin/renovation/application',
      name: 'application',
      meta: {
        title: '移动端首页',
        auth: true
      },
      component: _import('admin/renovation/application')
    },
    //任务大厅
    {
      path: '/task/taskswiper',
      name: 'taskswiper',
      meta: {
        title: '任务广告图',
        auth: true
      },
      component: _import('task/taskswiper')
    },
    {
      path: '/task/task',
      name: 'task',
      meta: {
        title: '任务大厅',
        auth: true
      },
      component: _import('task/task')
    },
    {
      path: '/task/addtask',
      name: 'addtask',
      meta: {
        title: '添加修改任务',
        auth: true
      },
      component: _import('task/task/addtask')
    },
    {
      path: '/task/taskcategory',
      name: 'taskcategory',
      meta: {
        title: '任务分类',
        auth: true
      },
      component: _import('task/taskcategory')
    },
    {
      path: '/task/taskstep',
      name: 'taskstep',
      meta: {
        title: '任务分类',
        auth: true
      },
      component: _import('task/taskstep')
    },
    //财务管理
    {
      path: 'storeStatistics',
      name: 'storeStatistics',
      meta: {
        title: '店铺统计',
        auth: true
      },
      component: _import('admin/accounting/storeStatistics')
    },
    {
      path: 'salesStatistics',
      name: 'salesStatistics',
      meta: {
        title: '销售统计',
        auth: true
      },
      component: _import('admin/accounting/salesStatistics')
    },
    //小说
    {
      path: 'novelCategory',
      name: 'novelCategory',
      meta: {
        title: '小说分类',
        auth: true
      },
      component: _import('novel/novelCategory')
    },
    {
      path: 'novel',
      name: 'novel',
      meta: {
        title: '小说列表',
        auth: true
      },
      component: _import('novel/novel')
    },
    {
      path: 'chapter',
      name: 'chapter',
      meta: {
        title: '小说章节',
        auth: true
      },
      component: _import('novel/chapter')
    },
    {
      path: 'addChapter',
      name: 'addChapter',
      meta: {
        title: '添加小说',
        auth: true
      },
      component: _import('novel/chapter/addChapter')
    },
    {
      path: 'novel/gather',
      name: 'gather',
      meta: {
        title: '小说采集',
        auth: true
      },
      component: _import('novel/gather')
    },
    //解题
    {
      path: 'examination',
      name: 'examination',
      meta: {
        title: '试卷列表',
        auth: true
      },
      component: _import('answer/examination')
    },
    {
      path: 'addExamination',
      name: 'addExamination',
      meta: {
        title: '添加试卷',
        auth: true
      },
      component: _import('answer/examination/addExamination')
    },
    {
      path: 'examinationPaper',
      name: 'examinationPaper',
      meta: {
        title: '试卷预览',
        auth: true
      },
      component: _import('answer/examination/examinationPaper')
    },
    {
      path: '/customer/servicemsg',
      name: 'servicemsg',
      meta: {
        title: '客服管理',
        auth: true
      },
      component: _import('customer/servicemsg')
    },
    {
      path: 'answertypes',
      name: 'answertypes',
      meta: {
        title: '综合类型',
        auth: true
      },
      component: _import('answer/answertypes')
    },
    //社区管理
    {
      path: 'forum/forumCategory',
      name: 'forumCategory',
      meta: {
        title: '论坛分类',
        auth: true
      },
      component: _import('socialize/forum/forumCategory')
    },
    {
      path: 'forum/forumPost',
      name: 'forumPost',
      meta: {
        title: '帖子管理',
        auth: true
      },
      component: _import('socialize/forum/forumPost')
    },
    {
      path: 'forum/addPost',
      name: 'addPost',
      meta: {
        title: '添加/编辑帖子',
        auth: true
      },
      component: _import('socialize/forum/addPost')
    },
    {
      path: 'forum/forumReply',
      name: 'forumReply',
      meta: {
        title: '评论管理',
        auth: true
      },
      component: _import('socialize/forum/forumReply')
    },
    {
      path: 'record/recordList',
      name: 'recordList',
      meta: {
        title: '相册管理',
        auth: true
      },
      component: _import('socialize/record/recordList')
    },
    {
      path: 'record/recordCategory',
      name: 'recordCategory',
      meta: {
        title: '相册分类',
        auth: true
      },
      component: _import('socialize/record/recordCategory')
    },
    {
      path: 'record/addRecord',
      name: 'addRecord',
      meta: {
        title: '记录分类',
        auth: true
      },
      component: _import('socialize/record/addRecord')
    },
    {
      path: 'record/forumSet',
      name: 'forumSet',
      meta: {
        title: '记录分类',
        auth: true
      },
      component: _import('socialize/forum/forumSet')
    },
    //应用系统管理
    {
      path: 'application/application',
      name: 'application',
      meta: {
        title: '应用管理',
        auth: true
      },
      component: _import('application/application')
    },
    {
      path: 'application/addApplication',
      name: 'addApplication',
      meta: {
        title: '添加应用',
        auth: true
      },
      component: _import('application/application/addApplication')
    },
    {
      path: 'application/classification',
      name: 'classification',
      meta: {
        title: '应用分类',
        auth: true
      },
      component: _import('application/classification')
    },
    {
      path: 'technicaldoc/technicalCategory',
      name: 'technicalCategory',
      meta: {
        title: '文档分类',
        auth: true
      },
      component: _import('application/technicaldoc/technicalCategory')
    },
    {
      path: 'technicaldoc/technicaldoc',
      name: 'technicaldoc',
      meta: {
        title: '技术文档',
        auth: true
      },
      component: _import('application/technicaldoc/technicaldoc')
    },
    {
      path: 'technicaldoc/addTechnicaldoc',
      name: 'addTechnicaldoc',
      meta: {
        title: '添加文档',
        auth: true
      },
      component: _import('application/technicaldoc/addTechnicaldoc')
    },
    // 系统 前端日志
    {
      path: 'log',
      name: 'log',
      meta: {
        title: '前端日志',
        auth: true
      },
      component: _import('system/log')
    },
    // 刷新页面 必须保留
    {
      path: 'refresh',
      name: 'refresh',
      hidden: true,
      component: _import('system/function/refresh')
    },
    // 页面重定向 必须保留
    {
      path: 'redirect/:route*',
      name: 'redirect',
      hidden: true,
      component: _import('system/function/redirect')
    }
  ]
}]

/**
 * 在主框架之外显示
 */
const frameOut = [
  // 登录
  {
    path: '/login',
    name: 'login',
    component: _import('system/login')
  }
]

/**
 * 错误页面
 */
const errorPage = [{
  path: '*',
  name: '404',
  component: _import('system/error/404')
}]

// 导出需要显示菜单的
export const frameInRoutes = frameIn

// 重新组织后导出
export default [
  ...frameIn,
  ...frameOut,
  ...errorPage
]
