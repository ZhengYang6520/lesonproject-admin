import {
  uniqueId
} from 'lodash'
import util from '@/libs/util.js'

import {
  userInfoUrl
} from '@/utils/interface'

function supplementPath(menu) {
  return menu.map(e => ({
    ...e,
    path: e.path || uniqueId('d2-menu-empty-'),
    ...e.children ? {
      children: supplementPath(e.children)
    } : {}
  }))
}

// export const menuHeader = supplementPath([{
//     title: '财务管理',
//     icon: 'folder-o',
//     children: [{
//       title: '店铺统计',
//       path: '/storeStatistics',
//       icon: 'icon-baobiaoguanli'
//     }, {
//       title: '销售统计',
//       path: '/salesStatistics',
//       icon: 'icon-baobiaoguanli'
//     }]
//   },
//   // {
//   //   title: '装修',
//   //   icon: 'folder-o',
//   //   children: [{
//   //     title: '首页装修',
//   //     children: [{
//   //       path: '/admin/renovation/application',
//   //       title: '移动端首页',
//   //     }]
//   //   }]
//   // },
//   {
//     title: '客服管理',
//     icon: 'folder-o',
//     children: [{
//       title: '客服',
//       path: '/customer/servicemsg',
//     }]
//   }
// ])

export const menuAside = supplementPath([{
    path: '/index',
    title: '首页',
    icon: 'home'
  },
  {
    title: '产品管理',
    icon: 'shopping-bag',
    children: [{
        path: '/product',
        title: '产品类表',
        icon: 'shopping-basket',
      },
      {
        path: '/productBrand',
        title: '品牌管理',
        icon: 'folder-open',
      },
      {
        path: '/productCategory',
        title: '分类管理',
        icon: 'bars',
      },
      {
        path: '/product/mallSet',
        title: '商品相关',
        icon: 'bars',
      }
    ]
  },
  // {
  //   title: '促销活动',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/promotion/lottery',
  //       title: '摇奖',
  //     },
  //     {
  //       path: '/promotion/coupon',
  //       title: '优惠券',
  //     },
  //     {
  //       path: '/promotion/pointsmall',
  //       title: '积分兑换',
  //     },
  //     {
  //       path: '/promotion/gift',
  //       title: '礼品',
  //     }
  //   ]
  // },
  {
    title: '推广管理',
    icon: 'folder-o',
    children: [
      //{
      //     path: '/distribution/commissionReport',
      //     title: '分销报表',
      //   },
      // {
      //   path: '/distribution/distributionExamine',
      //   title: '提现审核',
      // },
      {
        path: '/distribution/distributors',
        title: '推广人员',
      },
      // {
      //   path: '/distribution/distributionManage',
      //   title: '分销设置',
      // }
    ]
  },
  {
    title: '广告管理',
    icon: 'folder-o',
    children: [{
        path: '/picture/advertising',
        title: '广告管理',
      },
      {
        path: '/picture/classification',
        title: '广告分类',
      }
    ]
  },
  // {
  //   title: '应用管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/application/application',
  //       title: '应用管理',
  //     },
  //     {
  //       path: '/application/classification',
  //       title: '应用分类',
  //     },
  //     {
  //       path: '/technicaldoc/technicaldoc',
  //       title: '技术文档',
  //     },
  //     {
  //       path: '/technicaldoc/technicalCategory',
  //       title: '文档分类',
  //     }
  //   ]
  // },
  // {
  //   title: '抢购大厅',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/takemoney/takemoney',
  //       title: '抢购列表',
  //     },
  //     {
  //       path: '/takemoney/diamond',
  //       title: '钻石列表',
  //     },
  //     {
  //       path: '/takemoney/diamond/diamondprice',
  //       title: '钻石价格',
  //     },
  //     {
  //       path: '/takemoney/moneySetting',
  //       title: '抢购设置',
  //     }
  //   ]
  // },
  // {
  //   title: '任务大厅',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/task/taskswiper',
  //       title: '任务广告',
  //     }, {
  //       path: '/task/taskCategory',
  //       title: '任务分类',
  //     }, {
  //       path: '/task/task',
  //       title: '任务列表',
  //     },
  //     {
  //       path: '/task/taskstep',
  //       title: '会员任务',
  //     }
  //   ]
  // },
  {
    title: '订单管理',
    icon: 'folder-o',
    children: [{
      path: '/productOrder',
      title: '产品订单',
    }]
  },
  // {
  //   title: '文章管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/articleList',
  //       title: '文章列表',
  //     },
  //     {
  //       path: '/articleCategory',
  //       title: '文章分类',
  //       icon: 'bars',
  //     }, {
  //       path: '/newsList',
  //       title: '新闻列表',
  //     },
  //     {
  //       path: '/newsCategory',
  //       title: '新闻分类',
  //       icon: 'bars',
  //     }
  //   ]
  // },
  // {
  //   title: '小说管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/chapter',
  //       title: '小说章节',
  //     },
  //     {
  //       path: '/novel',
  //       title: '小说列表',
  //       icon: 'bars',
  //     },
  //     {
  //       path: '/novelCategory',
  //       title: '小说分类',
  //       icon: 'bars',
  //     },
  //     // {
  //     //   path: '/novel/gather',
  //     //   title: '小说采集',
  //     //   icon: 'bars',
  //     // }
  //   ]
  // },
  // {
  //   title: '解题管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/examination',
  //       title: '试卷列表',
  //     },
  //     {
  //       path: '/answertypes',
  //       title: '解题类型',
  //     }
  //   ]
  // },
  // {
  //   title: '多商户管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/merchant',
  //       title: '商户列表',
  //     },
  //     {
  //       path: '/merchantNature',
  //       title: '店铺性质',
  //     },
  //     {
  //       path: '/merchantLevel',
  //       title: '店铺等级',
  //     }
  //   ]
  // },
  // {
  //   title: '社区管理',
  //   icon: 'folder-o',
  //   children: [{
  //       path: '/forum/forumPost',
  //       title: '帖子管理',
  //     },
  //     {
  //       path: '/forum/forumReply',
  //       title: '评论管理',
  //     },
  //     {
  //       path: '/forum/forumCategory',
  //       title: '论坛分类',
  //     }, {
  //       path: '/record/recordList',
  //       title: '相册管理',
  //     }, {
  //       path: '/record/recordCategory',
  //       title: '相册分类',
  //     },
  //     {
  //       path: '/record/forumSet',
  //       title: '社区设置',
  //     }
  //   ]
  // },
  {
    title: '会员管理',
    icon: 'folder-o',
    children: [{
        path: '/userList',
        title: '会员列表',
      },
      {
        path: '/userLevel',
        title: '会员等级',
      },
      {
        path: '/users/userSet',
        title: '会员设置',
      }
    ]
  },
  {
    title: '管理员',
    icon: 'folder-o',
    children: [{
        path: '/adminList',
        title: '管理员',
      },
      {
        path: '/duties',
        title: '职务',
      }
    ]
  },
  {
    title: '站点设置',
    icon: 'folder-o',
    children: [{
        path: '/settings',
        title: '站点设置',
      },
      {
        path: '/system/notice',
        title: '站点公告',
      },
      // {
      //   path: '/website/onlineSetting',
      //   title: '网赚设置',
      // },
      // {
      //   path: '/website/siteGuide',
      //   title: '网务管理',
      // },
      // {
      //   path: '/website/mobileRouting',
      //   title: '移动端路由',
      // },
      // {
      //   path: '/website/sensitiveWord',
      //   title: '敏感词设置',
      // },
      // {
      //   path: '/payment',
      //   title: '支付设置',
      // },
      {
        path: '/commonarea',
        title: '地区设置',
      }
    ]
  }
])
