import {
  uniqueId
} from 'lodash'

import {
  mapState
} from 'vuex';

function supplementPath(menu) {
  return menu.map(e => ({
    ...e,
    path: e.path || uniqueId('d2-menu-empty-'),
    ...e.children ? {
      children: supplementPath(e.children)
    } : {}
  }))
}

export const menuHeader = supplementPath([{
    title: '财务管理',
    icon: 'folder-o',
    children: [{
      title: '销售统计',
      path: '/salesStatistics',
      icon: 'icon-baobiaoguanli'
    }]
  },
  {
    title: '装修',
    icon: 'folder-o',
    children: [{
      title: '首页装修',
      children: [{
        path: '/admin/renovation/application',
        title: '移动端首页'
      }]
    }]
  }
])

export const menuAside = supplementPath([{
    path: '/index',
    title: '首页',
    icon: 'home'
  },
  {
    title: '产品管理',
    icon: 'shopping-bag',
    children: [{
        path: '/product',
        title: '产品类表',
        icon: 'shopping-basket',
      },
      {
        path: 'productBrand',
        title: '品牌管理',
        icon: 'folder-open',
      },
      {
        path: 'productCategory',
        title: '分类管理',
        icon: 'bars'
      }
    ]
  },
  {
    title: '促销活动',
    icon: 'folder-o',
    children: [{
      path: 'lottery',
      title: '摇奖'
    }]
  },
  {
    title: '图片管理',
    icon: 'folder-o',
    children: [{
        path: 'advertising',
        title: '轮播广告'
      },
      {
        path: 'classification',
        title: '图片分类'
      },
    ]
  },
  {
    title: '订单管理',
    icon: 'folder-o',
    children: [{
      path: '/productOrder',
      title: '产品订单'
    }]
  },
  {
    title: '文章管理',
    icon: 'folder-o',
    children: [{
        path: '/articleList',
        title: '文章列表'
      },
      {
        path: '/articleCategory',
        title: '文章分类',
        icon: 'bars'
      }
    ]
  },
  {
    title: '小说管理',
    icon: 'folder-o',
    children: [{
        path: '/chapter',
        title: '小说章节'
      },
      {
        path: '/novel',
        title: '小说列表',
        icon: 'bars'
      },
      {
        path: '/novelCategory',
        title: '小说分类',
        icon: 'bars'
      }
    ]
  },
  {
    title: '解题管理',
    icon: 'folder-o',
    children: [{
      path: '/examination',
      title: '试卷列表'
    }, {
      path: '/answertypes',
      title: '解题类型'
    }]
  },
  {
    title: '多商户管理',
    icon: 'folder-o',
    children: [{
        path: '/merchant',
        title: '商户列表'
      }, {
        path: '/merchantNature',
        title: '店铺性质'
      },
      {
        path: '/merchantLevel',
        title: '店铺等级'
      }
    ]
  },
  {
    title: '会员管理',
    icon: 'folder-o',
    children: [{
        path: '/userList',
        title: '会员列表'
      },
      {
        path: '/userLevel',
        title: '会员等级'
      }
    ]
  },
  {
    title: '管理员',
    icon: 'folder-o',
    children: [{
        path: '/adminList',
        title: '管理员'
      },
      {
        path: '/duties',
        title: '职务'
      }
    ]
  },
  {
    title: '站点设置',
    icon: 'folder-o',
    children: [{
        path: '/settings',
        title: '站点设置'
      },
      {
        path: '/payment',
        title: '支付设置'
      },
      {
        path: '/commonarea',
        title: '地区设置'
      },
    ]
  }
])