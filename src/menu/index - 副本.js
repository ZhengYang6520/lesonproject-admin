import {
  uniqueId
} from 'lodash'
import util from '@/libs/util.js'

import {
  userInfoUrl
} from '@/utils/interface'

function supplementPath(menu) {
  return menu.map(e => ({
    ...e,
    path: e.path || uniqueId('d2-menu-empty-'),
    show: e.hasOwnProperty('show') ? e.show : true,
    ...(e.children && e.children.length > 0) ? {
      children: supplementPath(e.children)
    } : {}
  })).filter(e => e.show)
}

let moduleauths = null;
if (util.cookies.get('moduleauths')) {
  moduleauths = JSON.parse(util.cookies.get('moduleauths'));
  console.log(moduleauths);
} else {
  moduleauths = {
    is_product: false,
    is_productmanag: false,
    is_productbrand: false,
    is_productcategory: false,
    is_promotion: false,
    is_lottery: false,
    is_picture: false,
    is_adver: false,
    is_classification: false,
    is_order: false,
    is_proOrder: false,
    is_content: false,
    is_articleClass: false,
    is_article: false,
    is_newsCategory: false,
    is_news: false,
    is_managenovel: false,
    is_chapter: false,
    is_novel: false,
    is_novelCategory: false,
    is_answer: false,
    is_merchant: false,
    is_users: false,
    is_website: false,
    is_finance: false
  }
  console.log(moduleauths);
}

export const menuHeader = supplementPath([{
    title: '财务管理',
    icon: 'folder-o',
    children: [{
      title: '店铺统计',
      path: '/storeStatistics',
      icon: 'icon-baobiaoguanli'
    }, {
      title: '销售统计',
      path: '/salesStatistics',
      icon: 'icon-baobiaoguanli'
    }]
  },
  {
    title: '装修',
    icon: 'folder-o',
    show: true,
    children: [{
      title: '首页装修',
      show: true,
      children: [{
        path: '/admin/renovation/application',
        title: '移动端首页',
        show: true,
      }]
    }]
  },
  {
    title: '客服管理',
    icon: 'folder-o',
    show: true,
    children: [{
      title: '客服',
      path: '/customer/servicemsg',
      show: true
    }]
  }
])

export const menuAside = supplementPath([{
    path: '/index',
    title: '首页',
    icon: 'home'
  },
  {
    title: '产品管理',
    icon: 'shopping-bag',
    show: moduleauths.is_product,
    children: [{
        path: '/product',
        title: '产品类表',
        icon: 'shopping-basket',
        show: moduleauths.is_productmanag,
      },
      {
        path: '/productBrand',
        title: '品牌管理',
        icon: 'folder-open',
        show: moduleauths.is_productbrand,
      },
      {
        path: '/productCategory',
        title: '分类管理',
        icon: 'bars',
        show: moduleauths.is_productcategory,
      }
    ]
  },
  {
    title: '促销活动',
    icon: 'folder-o',
    show: moduleauths.is_promotion,
    children: [{
      path: '/lottery',
      title: '摇奖',
      show: moduleauths.is_lottery,
    }]
  },
  {
    title: '广告管理',
    icon: 'folder-o',
    show: moduleauths.is_picture,
    children: [{
        path: '/picture/advertising',
        title: '广告管理',
        show: moduleauths.is_adver,
      },
      {
        path: '/picture/classification',
        title: '广告分类',
        show: moduleauths.is_classification,
      }
    ]
  },
  {
    title: '订单管理',
    icon: 'folder-o',
    show: moduleauths.is_order,
    children: [{
      path: '/productOrder',
      title: '产品订单',
      show: moduleauths.is_proOrder,
    }]
  },
  {
    title: '文章管理',
    icon: 'folder-o',
    show: moduleauths.is_content,
    children: [{
        path: '/articleList',
        title: '文章列表',
        show: moduleauths.is_article,
      },
      {
        path: '/articleCategory',
        title: '文章分类',
        icon: 'bars',
        show: moduleauths.is_articleClass,
      }, {
        path: '/newsList',
        title: '新闻列表',
        show: moduleauths.is_news,
      },
      {
        path: '/newsCategory',
        title: '新闻分类',
        icon: 'bars',
        show: moduleauths.is_newsCategory,
      }
    ]
  },
  {
    title: '小说管理',
    icon: 'folder-o',
    show: moduleauths.is_managenovel,
    children: [{
        path: '/chapter',
        title: '小说章节',
        show: moduleauths.is_chapter,
      },
      {
        path: '/novel',
        title: '小说列表',
        icon: 'bars',
        show: moduleauths.is_novel,
      },
      {
        path: '/novelCategory',
        title: '小说分类',
        icon: 'bars',
        show: moduleauths.is_novelCategory,
      }
    ]
  },
  {
    title: '解题管理',
    icon: 'folder-o',
    show: true,
    children: [{
        path: '/examination',
        title: '试卷列表',
        show: true,
      },
      {
        path: '/answertypes',
        title: '解题类型',
        show: true,
      }
    ]
  },
  {
    title: '多商户管理',
    icon: 'folder-o',
    show: true,
    children: [{
        path: '/merchant',
        title: '商户列表',
        show: true,
      },
      {
        path: '/merchantcategory',
        title: '行业类目',
        show: true,
      },
      {
        path: '/merchantNature',
        title: '店铺性质',
        show: true,
      },
      {
        path: '/merchantLevel',
        title: '店铺等级',
        show: true,
      }
    ]
  },
  {
    title: '会员管理',
    icon: 'folder-o',
    show: true,
    children: [{
        path: '/userList',
        title: '会员列表',
        show: true,
      },
      {
        path: '/userLevel',
        title: '会员等级',
        show: true,
      }
    ]
  },
  {
    title: '管理员',
    icon: 'folder-o',
    show: true,
    children: [{
        path: '/adminList',
        title: '管理员',
        show: true,
      },
      {
        path: '/duties',
        title: '职务',
        show: true,
      }
    ]
  },
  {
    title: '站点设置',
    icon: 'folder-o',
    show: true,
    children: [{
        path: '/settings',
        title: '站点设置',
        show: true
      },
      {
          path: '/website/mobileRouting',
          title: '移动端路由',
          show: true
        },
      {
        path: '/payment',
        title: '支付设置',
        show: true
      },
      {
        path: '/commonarea',
        title: '地区设置',
        show: true
      }
    ]
  }
])
