const paniccategoryListUrl = '/admin/paniccategory/list/'
const panicbuyListUrl = '/admin/panicbuy/list/'
const panicbuyDetailUrl = '/admin/panicbuy/detail/'
const panicbuyCreateUrl = '/admin/panicbuy/create/'
const panicbuyUpdateUrl = '/admin/panicbuy/update/'
const panicbuyDeleteUrl = '/admin/panicbuy/delete/'
const panicbuyPatchUrl = '/admin/panicbuy/patch/'
const openlotteryUrl = '/admin/openlottery/'
const generateRobotUrl = '/admin/generate-robot-users/'

const marketpriceListUrl = '/admin/marketprice/list/'
const marketpriceDetailUrl = '/admin/marketprice/detail/'
const marketpriceCreateUrl = '/admin/marketprice/create/'
const marketpriceUpdateUrl = '/admin/marketprice/update/'
const marketpriceDeleteUrl = '/admin/marketprice/delete/'

const moneysettingDetailUrl = '/admin/moneysetting/detail/1/'
const moneysettingUpdateUrl = '/admin/moneysetting/update/1/'
const moneysettingPatchUrl = '/admin/moneysetting/patch/1/'

export {
  moneysettingPatchUrl,
  moneysettingUpdateUrl,
  moneysettingDetailUrl,
  marketpriceDeleteUrl,
  marketpriceUpdateUrl,
  marketpriceCreateUrl,
  marketpriceDetailUrl,
  marketpriceListUrl,
  generateRobotUrl,
  openlotteryUrl,
  paniccategoryListUrl,
  panicbuyListUrl,
  panicbuyDetailUrl,
  panicbuyCreateUrl,
  panicbuyUpdateUrl,
  panicbuyDeleteUrl,
  panicbuyPatchUrl
}
