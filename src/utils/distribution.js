const distributorsListUrl = '/admin/distributors/list/'
const distributorsDetailUrl = '/admin/distributors/detail/'
const distributorsCreateUrl = '/admin/distributors/create/'
const distributorsUpdateUrl = '/admin/distributors/update/'
const distributorsPatchUrl = '/admin/distributors/patch/'
const distributorsDeleteUrl = '/admin/distributors/delete/'

const distributorssettingsDetailUrl = '/admin/distributorssettings/detail/'
const distributorssettingsUpdateUrl = '/admin/distributorssettings/update/'
const distributorsSubUrl = '/admin/distributors/sub/'
const distributiondataListUrl = '/admin/distributiondata/list/'
const adminuserListUrl = '/admin/userList/'
const distributionstatsUrl = '/admin/distributionstats/'
const distributiondataPatchUrl = '/admin/distributiondata/patch/'

const identityListUrl = '/admin/identity/list/'
const identityDetailUrl = '/admin/identity/detail/'
const identityCreateUrl = '/admin/identity/create/'
const identityUpdateUrl = '/admin/identity/update/'
const identityDeleteUrl = '/admin/identity/delete/'

export {
  identityDeleteUrl,
  identityUpdateUrl,
  identityDetailUrl,
  identityListUrl,
  distributorsListUrl,
  distributorsDetailUrl,
  distributorsCreateUrl,
  distributorsUpdateUrl,
  distributorsPatchUrl,
  distributorsDeleteUrl,
  distributorssettingsDetailUrl,
  distributorssettingsUpdateUrl,
  distributorsSubUrl,
  distributiondataListUrl,
  adminuserListUrl,
  distributionstatsUrl,
  distributiondataPatchUrl
}
