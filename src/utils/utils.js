export function calculateFileSize(file) {
  if (file === null || file === undefined) {
    return null;
  }
  if (file < 1024) {
    return `${file}B`;
  } else if (file < 1024 * 1024) {
    return `${(file / 1024).toFixed(2)}KB`;
  } else if (file < 1024 * 1024 * 1024) {
    return `${(file / (1024 * 1024)).toFixed(2)}MB`;
  } else {
    return `${(file / (1024 * 1024 * 1024)).toFixed(2)}GB`;
  }
}
export function calculateNumber(num) {
  if (num >= 10000) {
    let result = (num / 10000).toFixed(2);
    return `${result}万`;
  } else {
    return num.toString();
  }
}