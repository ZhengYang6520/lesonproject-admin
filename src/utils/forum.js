const forumcategorytreeUrl = '/admin/forumcategory/tree/'
const forumcategoryListUrl = '/admin/forumcategory/list/'
const forumcategoryDetailUrl = '/admin/forumcategory/detail/'
const forumcategoryCreateUrl = '/admin/forumcategory/create/'
const forumcategoryUpdateUrl = '/admin/forumcategory/update/'
const forumcategoryDeleteUrl = '/admin/forumcategory/delete/'

const forumpostListUrl = '/admin/forumpost/list/'
const forumpostDetailUrl = '/admin/forumpost/detail/'
const forumpostCreateUrl = '/admin/forumpost/create/'
const forumpostUpdateUrl = '/admin/forumpost/update/'
const forumpostDeleteUrl = '/admin/forumpost/delete/'

const forumreplyListUrl = '/admin/forumreply/list/'
const forumreplyDeleteUrl = '/admin/forumreply/delete/'

export{
  forumcategorytreeUrl,
  forumcategoryListUrl,
  forumcategoryDetailUrl,
  forumcategoryCreateUrl,
  forumcategoryUpdateUrl,
  forumcategoryDeleteUrl,

  forumpostListUrl,
  forumpostDetailUrl,
  forumpostCreateUrl,
  forumpostUpdateUrl,
  forumpostDeleteUrl,
  
  forumreplyListUrl,
  forumreplyDeleteUrl
}
