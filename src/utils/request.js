import axios from 'axios'
import {
  Message
} from 'element-ui'
import util from '@/libs/util'
//export const baseURL = 'http://127.0.0.1:8000'
//export const baseURL = 'https://zhongxiang.createbei.com'
//export const baseURL = 'https://beigive.createbei.com'
export const baseURL = 'https://elianto.createbei.com'
const service = axios.create({
  baseURL,
  timeout: 555000,
  withCredentials: true
})
service.interceptors.request.use(
  config => {
    const token = util.cookies.get('token')
    if (token) {
      config.headers.Authorization = `Bearer ${token}`
    }
    config.headers['Content-Type'] = 'application/json;charset=UTF-8'
    return config
  },
  error => {
    return Promise.reject(error)
  }
)
service.interceptors.response.use(
  response => {
    return response
  },
  error => {
    return Promise.reject(error)
  }
)
export function get(url, params) {
  return service({
    url,
    method: 'get',
    params
  })
}
export function post(url, data) {
  return service({
    url,
    method: 'post',
    data
  })
}
export function put(url, data) {
  return service({
    url,
    method: 'put',
    data
  })
}
export function del(url, data) {
  return service({
    url,
    method: 'delete',
    data
  })
}
export function patch(url, data) {
  return service({
    url,
    method: 'patch',
    data
  })
}
