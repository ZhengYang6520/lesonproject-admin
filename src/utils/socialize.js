const recordcategoryTreeUrl = '/admin/recordcategory/tree/'
const recordcategoryListUrl = '/admin/recordcategory/list/'
const recordcategoryDetailUrl = '/admin/recordcategory/detail/'
const recordcategoryCreateUrl = '/admin/recordcategory/create/'
const recordcategoryUpdateUrl = '/admin/recordcategory/update/'
const recordcategoryDeleteUrl = '/admin/recordcategory/delete/'

const recordListUrl = '/admin/record/list/'
const recordDetailUrl = '/admin/record/detail/'
const recordCreateUrl = '/admin/record/create/'
const recordUpdateUrl = '/admin/record/update/'
const recordDeleteUrl = '/admin/record/delete/'

export {
  recordcategoryTreeUrl,
  recordcategoryListUrl,
  recordcategoryDetailUrl,
  recordcategoryCreateUrl,
  recordcategoryUpdateUrl,
  recordcategoryDeleteUrl,

  recordListUrl,
  recordDetailUrl,
  recordCreateUrl,
  recordUpdateUrl,
  recordDeleteUrl
}