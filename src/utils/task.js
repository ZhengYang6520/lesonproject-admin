const taskswiperListUrl = '/admin/taskswiper/list/'
const taskswiperDetailUrl = '/admin/taskswiper/detail/'
const taskswiperCreateUrl = '/admin/taskswiper/create/'
const taskswiperUpdateUrl = '/admin/taskswiper/update/'
const taskswiperDeleteUrl = '/admin/taskswiper/delete/'

const taskcategoryListUrl = '/admin/taskcategory/list/'
const taskcategoryDetailUrl = '/admin/taskcategory/detail/'
const taskcategoryCreateUrl = '/admin/taskcategory/create/'
const taskcategoryUpdateUrl = '/admin/taskcategory/update/'
const taskcategoryDeleteUrl = '/admin/taskcategory/delete/'

const taskListUrl = '/admin/task/list/'
const taskDetailUrl = '/admin/task/detail/'
const taskCreateUrl = '/admin/task/create/'
const taskUpdateUrl = '/admin/task/update/'
const taskDeleteUrl = '/admin/task/delete/'

const taskstepListUrl = '/admin/taskstep/list/'
const taskstepDetailUrl = '/admin/taskstep/detail/'
const taskstepCreateUrl = '/admin/taskstep/create/'
const taskstepUpdateUrl = '/admin/taskstep/update/'
const taskstepPatchUrl = '/admin/taskstep/patch/'
const taskstepDeleteUrl = '/admin/taskstep/delete/'



export{
  taskstepDeleteUrl,
  taskstepUpdateUrl,
  taskstepPatchUrl,
  taskstepCreateUrl,
  taskstepDetailUrl,
  taskstepListUrl,
  taskswiperListUrl,
  taskswiperDetailUrl,
  taskswiperCreateUrl,
  taskswiperUpdateUrl,
  taskswiperDeleteUrl,

  taskcategoryListUrl,
  taskcategoryDetailUrl,
  taskcategoryCreateUrl,
  taskcategoryUpdateUrl,
  taskcategoryDeleteUrl,

  taskListUrl,
  taskDetailUrl,
  taskCreateUrl,
  taskUpdateUrl,
  taskDeleteUrl,
}
