const classificationListUrl = '/classification/list/'

const classificationDetailUrl = '/classification/detail/'

const classificationCreateUrl = '/classification/create/'

const classificationUpdateUrl = '/classification/update/'

const classificationDeleteUrl = '/classification/delete/'

const advertisingListUrl = '/advertising/list/'

const advertisingDetailUrl = '/advertising/detail/'

const advertisingCreateUrl = '/advertising/create/'

const advertisingUpdateUrl = '/advertising/update/'

const advertisingDeleteUrl = '/advertising/delete/'

export {
  classificationListUrl,
  classificationDetailUrl,
  classificationCreateUrl,
  classificationUpdateUrl,
  classificationDeleteUrl,
  advertisingListUrl,
  advertisingDetailUrl,
  advertisingCreateUrl,
  advertisingUpdateUrl,
  advertisingDeleteUrl,
}
