const technicalCategoryListUrl = '/admin/technicalcategory/list/'
const technicalCategoryDetailUrl = '/admin/technicalcategory/detail/'
const technicalCategoryCreateUrl = '/admin/technicalcategory/create/'
const technicalCategoryUpdateUrl = '/admin/technicalcategory/update/'
const technicalCategoryDeleteUrl = '/admin/technicalcategory/delete/'
const technicalListUrl = '/admin/technical/list/'
const technicalDetailUrl = '/admin/technical/detail/'
const technicalCreateUrl = '/admin/technical/create/'
const technicalUpdateUrl = '/admin/technical/update/'
const technicalDeleteUrl = '/admin/technical/delete/'

export {
  technicalCategoryListUrl,
  technicalCategoryDetailUrl,
  technicalCategoryCreateUrl,
  technicalCategoryUpdateUrl,
  technicalCategoryDeleteUrl,
  technicalListUrl,
  technicalDetailUrl,
  technicalCreateUrl,
  technicalUpdateUrl,
  technicalDeleteUrl,
}
