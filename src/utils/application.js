const softwareListUrl = '/admin/software/list/'

const softwareDetailUrl = '/admin/software/detail/'

const softwareCreateUrl = '/admin/software/create/'

const softwareUpdateUrl = '/admin/software/update/'

const softwareDeleteUrl = '/admin/software/delete/'

const classapplicationListUrl = '/admin/classapplication/list/'

const classapplicationDetailUrl = '/admin/classapplication/detail/'

const classapplicationCreateUrl = '/admin/classapplication/create/'

const classapplicationUpdateUrl = '/admin/classapplication/update/'

const classapplicationDeleteUrl = '/admin/classapplication/delete/'

export{
  classapplicationListUrl,
  classapplicationDetailUrl,
  classapplicationCreateUrl,
  classapplicationUpdateUrl,
  classapplicationDeleteUrl,
  softwareListUrl,
  softwareDetailUrl,
  softwareCreateUrl,
  softwareUpdateUrl,
  softwareDeleteUrl ,
}
