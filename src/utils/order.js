const orderListUrl = 'admin/orderList/'

const orderDeleteUrl = '/admin/order/delete/'

const orderDetailUrl = '/admin/orderDetail/'

const orderPatchUrl = '/admin/order/patch/'

export {
  orderListUrl,
  orderDeleteUrl,
  orderDetailUrl,
  orderPatchUrl
}