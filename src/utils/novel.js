const novelcategoryTreeUrl = '/novelcategory/tree/'

const novelcategoryListUrl = '/novelcategory/list/'

const novelcategoryCreateUrl = '/novelcategory/create/'

const novelcategoryDeleteUrl = '/novelcategory/delete/'

const novelcategoryDetailUrl = '/novelcategory/detail/'

const novelcategoryUpdateUrl = '/novelcategory/update/'

const novelListUrl = '/novel/list/'

const novelCreateUrl = '/novel/create/'

const novelDeleteUrl = '/novel/delete/'

const novelDetailUrl = '/novel/detail/'

const novelUpdateUrl = '/novel/update/'

const chapterListUrl = '/chapter/list/'

const chapterDetailUrl = '/chapter/detail/'

const chapterCreateUrl = '/chapter/create/'

const chapterUpdateUrl = '/chapter/update/'

const chapterDeleteUrl = '/chapter/delete/'

const novelSearchUrl = '/admin/novel/search/'

const noveldetailsSearchUrl = '/admin/noveldetails/search/'

const chapterSearchUrl = '/admin/chapter/search/'

const chapterdetailsSearchUrl = '/admin/chapterdetails/search/'

export {
  chapterDeleteUrl,
  chapterUpdateUrl,
  chapterCreateUrl,
  chapterDetailUrl,
  chapterListUrl,
  novelUpdateUrl,
  novelDetailUrl,
  novelDeleteUrl,
  novelCreateUrl,
  novelListUrl,
  novelcategoryUpdateUrl,
  novelcategoryDetailUrl,
  novelcategoryDeleteUrl,
  novelcategoryCreateUrl,
  novelcategoryListUrl,
  novelcategoryTreeUrl,
  novelSearchUrl,
  noveldetailsSearchUrl,
  chapterSearchUrl,
  chapterdetailsSearchUrl
}
