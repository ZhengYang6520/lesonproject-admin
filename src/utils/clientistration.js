const moduleListUrl = '/admin/module/list/'
const moduleDetailUrl = '/admin/module/detail/'
const moduleCreateUrl = '/admin/module/create/'
const moduleUpdateUrl = '/admin/module/update/'
const moduleDeleteUrl = '/admin/module/delete/'

const navigationUrl = '/navigation/'

const applicationCreateUrl = '/admin/application/create/'
const applicationDetailUrl = '/admin/application/detail/'
const applicationUpdateUrl = '/admin/application/update/'

const routeclassTreeUrl = '/admin/routeclass/tree/'
const routeclassCreateUrl = '/admin/routeclass/create/'
const routeclassDetailUrl = '/admin/routeclass/detail/'
const routeclassUpdateUrl = '/admin/routeclass/update/'
const routeclassDeleteUrl = '/admin/routeclass/delete/'

const routeurlListUrl = '/admin/routeurl/list/'
const routeurlCreateUrl = '/admin/routeurl/create/'
const routeurlDetailUrl = '/admin/routeurl/detail/'
const routeurlUpdateUrl = '/admin/routeurl/update/'
const routeurlDeleteUrl = '/admin/routeurl/delete/'

const siteguideListUrl = '/admin/siteguide/list/'
const siteguideCreateUrl = '/admin/siteguide/create/'
const siteguideDetailUrl = '/admin/siteguide/detail/'
const siteguideUpdateUrl = '/admin/siteguide/update/'
const siteguideDeleteUrl = '/admin/siteguide/delete/'

const keywordListUrl = '/admin/keyword/list/'
const keywordCreateUrl = '/admin/keyword/create/'
const keywordDetailUrl = '/admin/keyword/detail/'
const keywordUpdateUrl = '/admin/keyword/update/'
const keywordDeleteUrl = '/admin/keyword/delete/'


export {
  keywordListUrl,
  keywordCreateUrl,
  keywordDetailUrl,
  keywordUpdateUrl,
  keywordDeleteUrl,
  moduleListUrl,
  moduleDetailUrl,
  moduleCreateUrl,
  moduleUpdateUrl,
  moduleDeleteUrl,
  navigationUrl,
  applicationCreateUrl,
  applicationDetailUrl,
  applicationUpdateUrl,
  routeclassTreeUrl,
  routeclassCreateUrl,
  routeclassDetailUrl,
  routeclassUpdateUrl,
  routeclassDeleteUrl,
  routeurlListUrl,
  routeurlCreateUrl,
  routeurlDetailUrl,
  routeurlUpdateUrl,
  routeurlDeleteUrl,
  siteguideListUrl,
  siteguideCreateUrl,
  siteguideDetailUrl,
  siteguideUpdateUrl,
  siteguideDeleteUrl,
}