const productCategoriesUrl = '/productCategories/'

const productCategoriesDetailUrl = '/productCategoryDetail/'

const productCategoriesTreeUrl = '/productCategories/tree/'

const productCategoriesCreateUrl = '/productCategories/create/'

const productCategoriesDeleteUrl = '/productCategories/delete/'

const productCategoriesUpdateUrl = '/productCategories/update/'

const productListUrl = 'admin/productList/'

const productsUrl = 'api/products/'

const productDetailUrl = '/api/productDetail/'

const productCreateUrl = '/admin/product/create/'

const productUpdateUrl = '/admin/product/update/'

const productPatchUrl = '/admin/product/patch/'

const productDeleteUrl = '/admin/product/delete/'


const propertiesListUrl = '/admin/properties/list/'

const propertiesDetailUrl = '/admin/properties/detail/'

const propertiesCreateUrl = '/admin/properties/create/'

const propertiesUpdateUrl = '/admin/properties/update/'

const propertiesDeleteUrl = '/admin/properties/delete/'

const attributevalueListUrl = '/admin/attributevalue/list/'

const attributevalueDetailUrl = '/admin/attributevalue/detail/'

const attributevalueCreateUrl = '/admin/attributevalue/create/'

const attributevalueUpdateUrl = '/admin/attributevalue/update/'

const attributevalueDeleteUrl = '/admin/attributevalue/delete/'

const brandsListUrl = 'api/brands/list/'

const brandsdetailUrl = 'api/brands/detail/'

const brandsCreateUrl = 'admin/brands/create/'

const brandsUpdateUrl = 'admin/brands/update/'

const brandsDeleteUrl = 'admin/brands/delete/'

const countryListUrl = 'api/country/list/'

const countrydetailUrl = 'api/country/detail/'

const countryCreateUrl = 'admin/country/create/'

const countryUpdateUrl = 'admin/country/update/'

const countryDeleteUrl = 'admin/country/delete/'

export {
  countryListUrl,
  countrydetailUrl,
  countryCreateUrl,
  countryUpdateUrl,
  countryDeleteUrl,
  brandsListUrl,
  brandsdetailUrl,
  brandsCreateUrl,
  brandsUpdateUrl,
  brandsDeleteUrl,
  attributevalueDeleteUrl,
  attributevalueUpdateUrl,
  attributevalueCreateUrl,
  attributevalueDetailUrl,
  attributevalueListUrl,
  propertiesDeleteUrl,
  propertiesUpdateUrl,
  propertiesCreateUrl,
  propertiesDetailUrl,
  propertiesListUrl,
  productListUrl,
  productsUrl,
  productCategoriesUrl,
  productCategoriesDetailUrl,
  productCategoriesTreeUrl,
  productCategoriesCreateUrl,
  productCategoriesDeleteUrl,
  productCategoriesUpdateUrl,
  productDetailUrl,
  productCreateUrl,
  productUpdateUrl,
  productPatchUrl,
  productDeleteUrl
}
