const merchantlevelListUrl = '/admin/merchantlevel/list/'

const merchantlevelDetailUrl = '/admin/merchantlevel/detail/'

const merchantlevelCreateUrl = '/admin/merchantlevel/create/'

const merchantlevelUpdateUrl = '/admin/merchantlevel/update/'

const merchantlevelDeleteUrl = '/admin/merchantlevel/delete/'

const merchantnatureListUrl = '/admin/merchantnature/list/'

const merchantnatureDetailUrl = '/admin/merchantnature/detail/'

const merchantnatureCreateUrl = '/admin/merchantnature/create/'

const merchantnatureUpdateUrl = '/admin/merchantnature/update/'

const merchantnatureDeleteUrl = '/admin/merchantnature/delete/'

const merchantListUrl = '/admin/merchant/list/'

const merchantPatchUrl = '/admin/merchant/patch/'

const merchantDetailUrl = '/admin/merchant/detail/'


export{
  merchantPatchUrl,
  merchantListUrl,
  merchantDetailUrl,
  merchantnatureDeleteUrl,
  merchantnatureUpdateUrl,
  merchantnatureCreateUrl,
  merchantnatureDetailUrl,
  merchantnatureListUrl,
  merchantlevelDeleteUrl,
  merchantlevelUpdateUrl,
  merchantlevelCreateUrl,
  merchantlevelDetailUrl,
  merchantlevelListUrl,
}
