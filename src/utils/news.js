const newscategoriesListUrl = '/api/newscategories/list/'

const newscategoriesDetailUrl = '/api/newscategory/detail/'

const newscategoriesCreateUrl = '/admin/newscategories/create/'

const newscategoriesUpdateUrl = '/admin/newscategories/update/'

const newscategoriesDeleteUrl = '/admin/categories/delete/'

const newsListUrl = '/admin/newsList/'

const newsCreateUrl = '/api/news/create/'

const newsDetailUrl = '/api/news/detail/'

const newsUpdateUrl = '/api/news/update/'

const newsDeleteUrl = '/api/news/delete/'

const articlecategoryListUrl = '/api/articlecategory/list/'

const articlecategoryDetailUrl = '/api/articlecategory/detail/'

const articlecategoryCreateUrl = '/admin/articlecategory/create/'

const articlecategoryUpdateUrl = '/admin/articlecategory/update/'

const articlecategoryDeleteUrl = '/admin/articlecategory/delete/'

const articleListUrl = '/api/article/list/'

const articleDetailUrl = '/api/article/detail/'

const articleCreateUrl = '/api/article/create/'

const articleUpdateUrl = '/api/article/update/'

const articleDeleteUrl = '/api/article/delete/'

export {
  newscategoriesListUrl,
  newscategoriesDetailUrl,
  newscategoriesCreateUrl,
  newscategoriesUpdateUrl,
  newscategoriesDeleteUrl,
  newsListUrl,
  newsCreateUrl,
  newsDetailUrl,
  newsUpdateUrl,
  newsDeleteUrl,
  articlecategoryListUrl,
  articlecategoryDetailUrl,
  articlecategoryCreateUrl,
  articlecategoryUpdateUrl,
  articlecategoryDeleteUrl,
  articleListUrl,
  articleDetailUrl,
  articleCreateUrl,
  articleUpdateUrl,
  articleDeleteUrl
}
