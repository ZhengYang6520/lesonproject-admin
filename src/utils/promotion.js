const lotterylListUrl = '/lottery/list/'

const lotterylDetailUrl = '/lottery/detail/'

const lotterylCreateUrl = '/lottery/create/'

const lotterylUpdateUrl = '/lottery/update/'

const lotterylDeleteUrl = '/lottery/delete/'

const prizeUrl = '/admin/prize/'

const couponListUrl = '/admin/coupon/list/'

const couponDeleteUrl = '/admin/coupon/delete/'

const couponCreateUrl = '/admin/coupon/create/'

const couponDetailUrl = '/admin/coupon/detail/'

const couponUpdateUrl = '/admin/coupon/update/'

const couponPatchUrl = '/admin/coupon/patch/'

const pointsmallListUrl = '/admin/pointsmall/list/'

const pointsmallCreateUrl = '/admin/pointsmall/create/'

const pointsmallDetailUrl = '/admin/pointsmall/detail/'

const pointsmallUpdateUrl = '/admin/pointsmall/update/'

const pointsmallDeleteUrl = '/admin/pointsmall/delete/'

const giftListUrl = '/admin/gift/list/'

const giftCreateUrl = '/admin/gift/create/'

const giftDetailUrl = '/admin/gift/detail/'

const giftUpdateUrl = '/admin/gift/update/'

const giftDeleteUrl = '/admin/gift/delete/'

export {
  prizeUrl,
  lotterylDeleteUrl,
  lotterylUpdateUrl,
  lotterylCreateUrl,
  lotterylDetailUrl,
  lotterylListUrl,
  couponListUrl,
  couponDeleteUrl,
  couponCreateUrl,
  couponDetailUrl,
  couponUpdateUrl,
  couponPatchUrl,
  pointsmallListUrl,
  pointsmallCreateUrl,
  pointsmallDetailUrl,
  pointsmallUpdateUrl,
  pointsmallDeleteUrl,
  giftListUrl,
  giftCreateUrl,
  giftDetailUrl,
  giftUpdateUrl,
  giftDeleteUrl,
}