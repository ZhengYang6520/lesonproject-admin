const areaListUrl = '/area/list/'

const areaDetailUrl = '/area/detail/'

const areaCreateUrl = '/area/create/'

const areaUpdateUrl = '/area/update/'

const areaDeleteUrl = '/area/delete/'

const courseListUrl = '/course/list/'

const courseDetailUrl = '/course/detail/'

const courseCreateUrl = '/course/create/'

const courseUpdateUrl = '/course/update/'

const courseDeleteUrl = '/course/delete/'

const editionListUrl = '/edition/list/'

const editionDetailUrl = '/edition/detail/'

const editionCreateUrl = '/edition/create/'

const editionUpdateUrl = '/edition/update/'

const editionDeleteUrl = '/edition/delete/'

const substanceListUrl = '/substance/list/'

const substanceDetailUrl = '/substance/detail/'

const substanceCreateUrl = '/substance/create/'

const substanceUpdateUrl = '/substance/update/'

const substanceDeleteUrl = '/substance/delete/'

const segmentListUrl = '/segment/list/'

const segmentDetailUrl = '/segment/detail/'

const segmentCreateUrl = '/segment/create/'

const segmentUpdateUrl = '/segment/update/'

const segmentDeleteUrl = '/segment/delete/'

const semesterListUrl = '/semester/list/'

const semesterDetailUrl = '/semester/detail/'

const semesterCreateUrl = '/semester/create/'

const semesterUpdateUrl = '/semester/update/'

const semesterDeleteUrl = '/semester/delete/'

const difficultyListUrl = '/difficulty/list/'

const difficultyDetailUrl = '/difficulty/detail/'

const difficultyCreateUrl = '/difficulty/create/'

const difficultyUpdateUrl = '/difficulty/update/'

const difficultyDeleteUrl = '/difficulty/delete/'

const synthesizeListUrl = '/synthesize/list/'

const synthesizeDetailUrl = '/synthesize/detail/'

const synthesizeCreateUrl = '/synthesize/create/'

const synthesizeUpdateUrl = '/synthesize/update/'

const synthesizeDeleteUrl = '/synthesize/delete/'

const examinationListUrl = '/examination/list/'

const examinationDetailUrl = '/examination/detail/'

const examinationCreateUrl = '/examination/create/'

const examinationUpdateUrl = '/examination/update/'

const examinationDeleteUrl = '/examination/delete/'

const topictypeListUrl = '/topictype/list/'

const topictypeDetailUrl = '/topictype/detail/'

const topictypeCreateUrl = '/topictype/create/'

const topictypeUpdateUrl = '/topictype/update/'

const topictypeDeleteUrl = '/topictype/delete/'

const topicListUrl = '/topic/list/'

const topicDetailUrl = '/topic/detail/'

const topicCreateUrl = '/topic/create/'

const topicUpdateUrl = '/topic/update/'

const topicDeleteUrl = '/topic/delete/'

const answeraboutListUrl = '/answerabout/list/'

const answeraboutDetailUrl = '/answerabout/detail/'

const answeraboutCreateUrl = '/answerabout/create/'

const answeraboutUpdateUrl = '/answerabout/update/'

const answeraboutDeleteUrl = '/answerabout/delete/'

export {
  answeraboutDeleteUrl,
  answeraboutUpdateUrl,
  answeraboutCreateUrl,
  answeraboutDetailUrl,
  answeraboutListUrl,
  topicDeleteUrl,
  topicUpdateUrl,
  topicCreateUrl,
  topicDetailUrl,
  topicListUrl,
  topictypeDeleteUrl,
  topictypeUpdateUrl,
  topictypeCreateUrl,
  topictypeDetailUrl,
  topictypeListUrl,
  examinationDeleteUrl,
  examinationUpdateUrl,
  examinationCreateUrl,
  examinationDetailUrl,
  examinationListUrl,
  synthesizeDeleteUrl,
  synthesizeUpdateUrl,
  synthesizeCreateUrl,
  synthesizeDetailUrl,
  synthesizeListUrl,
  difficultyDeleteUrl,
  difficultyUpdateUrl,
  difficultyCreateUrl,
  difficultyDetailUrl,
  difficultyListUrl,
  semesterDeleteUrl,
  semesterUpdateUrl,
  semesterCreateUrl,
  semesterDetailUrl,
  semesterListUrl,
  segmentDeleteUrl,
  segmentUpdateUrl,
  segmentCreateUrl,
  segmentDetailUrl,
  segmentListUrl,
  substanceDeleteUrl,
  substanceUpdateUrl,
  substanceCreateUrl,
  substanceDetailUrl,
  substanceListUrl,
  editionDeleteUrl,
  editionUpdateUrl,
  editionCreateUrl,
  editionDetailUrl,
  editionListUrl,
  courseDeleteUrl,
  courseUpdateUrl,
  courseCreateUrl,
  courseDetailUrl,
  courseListUrl,
  areaDeleteUrl,
  areaUpdateUrl,
  areaCreateUrl,
  areaDetailUrl,
  areaListUrl,
}
