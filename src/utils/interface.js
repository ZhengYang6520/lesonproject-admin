const userListUrl = 'admin/userList/'

const userUrl = '/api/users/'

const userDetailUrl = '/admin/userdetail/'

const userPatchUrl = '/admin/userPatch/'

const userInfoUrl = '/admin/user-info/'

const moduleauthCreateUrl = '/moduleauth/create/'

const moduleauthDetailUrl = '/moduleauth/detail/'

const moduleauthUpdateUrl = '/moduleauth/update/'

const dutiesUrl = '/admin/duties/'

const statsUrl = '/admin/stats/'

const productsalesUrl = '/admin/productsales/'

const newsCreateUrl = 'news/create/'

const applySettleListUrl = '/admin/applySettleList/'

const applyDetailUrl = '/admin/apply-settle/'

const imagesUploadUrl = '/images/upload/'

const imagesListUploadUrl = 'imagesList/upload/'

const brandsListUrl = '/api/brands/list/'

const brandsUrl = '/brands/'

const brandsCreateUrl = '/brands/create/'

const countryListUrl = '/country/list/'

const countryUrl = '/country/'

const websiteCeateUrl = '/admin/websites/ceate'

const websiteUpdateUrl  = '/admin/websites/update/'

const websitesListUrl = '/admin/websites/list/'

const websitesDetailUrl = '/admin/website/detail/'

const adminsAddUrl = '/admin/add/'

const adminLoginUrl = '/api/login/'

const lotteryUrl = '/lottery/'

const prizeUrl = '/prize/'

const paymentconfigListUrl = '/paymentconfig/list/'

const paymentconfigCreateUrl = '/paymentconfig/create/'

const paymentconfigUpdateUrl = '/paymentconfig/update/'

const paymentconfigDetailUrl = '/paymentconfig/detail/'

const commonareaListUrl = '/admin/commonarea/list/'

const businessstatsUrl = '/admin/businessstats/'

const memberlevelListUrl = '/admin/memberlevel/list/'

const memberlevelDetailUrl = 'admin/memberlevel/detail/'

const memberlevelCreateUrl = '/admin/memberlevel/create/'

const memberlevelUpdateUrl = '/admin/memberlevel/update/'

const memberlevelDeleteUrl = '/admin/memberlevely/delete/'

const usersetListUrl = '/admin/userset/list/'

const usersetDetailUrl = 'admin/userset/detail/'

const usersetCreateUrl = '/admin/userset/create/'

const usersetUpdateUrl = '/admin/userset/update/'

const usersetDeleteUrl = '/admin/userset/delete/'

const sensitivewordListUrl = '/admin/sensitiveword/list/'

const sensitivewordDetailUrl = 'admin/sensitiveword/detail/'

const sensitivewordCreateUrl = '/admin/sensitiveword/create/'

const sensitivewordUpdateUrl = '/admin/sensitiveword/update/'

const sensitivewordDeleteUrl = '/admin/sensitiveword/delete/'

const exportExcelUrl = '/admin/export-excel/'

const onlinesetListUrl = '/admin/onlineset/list/'

const onlinesetDetailUrl = 'admin/onlineset/detail/'

const onlinesetCreateUrl = '/admin/onlineset/create/'

const onlinesetUpdateUrl = '/admin/onlineset/update/'

const diamondpurchaseListUrl = '/admin/diamondpurchase/list/'

const diamondpurchaseDetailUrl = '/admin/diamondpurchase/detail/'

const diamondpurchaseCreateUrl = '/admin/diamondpurchase/create/'

const diamondpurchaseUpdateUrl = '/admin/diamondpurchase/update/'

const diamondpurchaseDeleteUrl = '/admin/diamondpurchase/delete/'

const noticeDetailUrl = '/admin/notice/detail/1/'

const noticeUpdateUrl = '/admin/notice/update/1/'

export {
  noticeUpdateUrl,
  noticeDetailUrl,
  diamondpurchaseDeleteUrl,
  diamondpurchaseUpdateUrl,
  diamondpurchaseCreateUrl,
  diamondpurchaseDetailUrl,
  diamondpurchaseListUrl,
  onlinesetListUrl,
  onlinesetDetailUrl,
  onlinesetCreateUrl,
  onlinesetUpdateUrl,
  sensitivewordListUrl,
  sensitivewordDetailUrl,
  sensitivewordCreateUrl,
  sensitivewordUpdateUrl,
  sensitivewordDeleteUrl,
  usersetListUrl,
  usersetDetailUrl,
  usersetCreateUrl,
  usersetUpdateUrl,
  usersetDeleteUrl,
  memberlevelListUrl,
  memberlevelDetailUrl,
  memberlevelCreateUrl,
  memberlevelUpdateUrl,
  memberlevelDeleteUrl,
  commonareaListUrl,
  userListUrl,
  userUrl,
  userDetailUrl,
  userPatchUrl,
  userInfoUrl,
  moduleauthCreateUrl,
  moduleauthDetailUrl,
  moduleauthUpdateUrl,
  dutiesUrl,
  statsUrl,
  productsalesUrl,
  newsCreateUrl,
  applySettleListUrl,
  applyDetailUrl,
  imagesUploadUrl,
  imagesListUploadUrl,
  brandsListUrl,
  brandsUrl,
  brandsCreateUrl,
  countryListUrl,
  countryUrl,
  websiteCeateUrl,
  websiteUpdateUrl,
  websitesListUrl,
  websitesDetailUrl,
  adminsAddUrl,
  adminLoginUrl,
  lotteryUrl,
  prizeUrl,
  paymentconfigListUrl,
  paymentconfigCreateUrl,
  paymentconfigUpdateUrl,
  paymentconfigDetailUrl,
  businessstatsUrl,
}
